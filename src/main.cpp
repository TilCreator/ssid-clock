#include <Arduino.h>
#include <WiFiManager.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <WiFi.h>
#include <WiFiAP.h>

WiFiManager wifiManager;

WiFiUDP ntp_udp;
// TODO Implement summer/winter time
NTPClient ntp_client(ntp_udp, "europe.pool.ntp.org", 3600, 360000);

int old_min = 0;

void setup() {
    Serial.begin(115200);

    Serial.println("start");

    wifiManager.autoConnect("🕛", "hackingIsAwesome");

    Serial.println("WiFi connected");

    ntp_client.setTimeOffset(2 * 60 * 60);

    ntp_client.begin();
}

void loop() {
    ntp_client.update();

    if (ntp_client.getMinutes() != old_min) {
        char clock_emoji[5];
        switch (ntp_client.getHours() % 12) {
            case 0:
                strncpy(clock_emoji, "🕛", 5);
                break;
            case 1:
                strncpy(clock_emoji, "🕐", 5);
                break;
            case 2:
                strncpy(clock_emoji, "🕑", 5);
                break;
            case 3:
                strncpy(clock_emoji, "🕒", 5);
                break;
            case 4:
                strncpy(clock_emoji, "🕓", 5);
                break;
            case 5:
                strncpy(clock_emoji, "🕔", 5);
                break;
            case 6:
                strncpy(clock_emoji, "🕕", 5);
                break;
            case 7:
                strncpy(clock_emoji, "🕖", 5);
                break;
            case 8:
                strncpy(clock_emoji, "🕗", 5);
                break;
            case 9:
                strncpy(clock_emoji, "🕘", 5);
                break;
            case 10:
                strncpy(clock_emoji, "🕙", 5);
                break;
            case 11:
                strncpy(clock_emoji, "🕚", 5);
                break;
            default:
                strncpy(clock_emoji, "🕛", 5);
                break;
        }

        char formated_time[13];
        sprintf(formated_time, "%s %02d:%02d", clock_emoji, ntp_client.getHours(), ntp_client.getMinutes());

        Serial.println(formated_time);
        WiFi.softAP(formated_time, NULL, 0, 0, 0);
    }

    delay(200);
}
